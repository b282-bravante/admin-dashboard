const express = require('express');
const router = express.Router();
const humanResourcesControllers = require ("../Controllers/humanResourcesControllers")

router.post ('/contributions', async (req, res) => {
    
    try{
        const resultFromController = await humanResourcesControllers.saveContributions(req.body);
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error (`failed to submit: ${err}`);
        res.status(500).json({error: "Internal Server Error"})
    }
    
})

router.get('/display/contributions', async (req, res) => {
    try {
        // Call the controller function to fetch TD status data
        const resultFromController = await humanResourcesControllers.displayContributions();
        console.log("Result from controller: ", resultFromController);

        // Send the fetched data back to the client as a JSON response
        res.status(200).json(resultFromController);
    } catch (err) {
        // Handle errors
        console.error("Failed to get: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
});


router.post('/count', async (req, res) => {
    try {
        const resultFromController = await humanResourcesControllers.saveCounts(req.body);
        console.log(`Result from controller: ${resultFromController}`);

        res.status(200).json(resultFromController);
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({error: "internal server error"})
    }
})

router.get('/display/count', async (req, res) => {
    try {
        // Call the controller function to fetch TD status data
        const resultFromController = await humanResourcesControllers.displayCount();
        console.log("Result from controller: ", resultFromController);

        // Send the fetched data back to the client as a JSON response
        res.status(200).json(resultFromController);
    } catch (err) {
        // Handle errors
        console.error("Failed to get: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})

router.post('/hiring', async (req, res) =>{
    try {
        const resultFromController = await humanResourcesControllers.saveHiring(req.body);
        console.log(`Result from controller: ${resultFromController}`);

        res.status(200).json(resultFromController);
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({error: "internal server error"})
    }
})
router.post('/worker_hiring', async (req, res) =>{
    try {
        const resultFromController = await humanResourcesControllers.saveWorkerHiring(req.body);
        console.log(`Result from controller: ${resultFromController}`);

        res.status(200).json(resultFromController);
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({error: "internal server error"})
    }
})


router.get('/display/hiring', async (req, res) => {
    try {
        const resultFromController = await humanResourcesControllers.displayHiring();
        console.log(`Result from controller: ${resultFromController}`);

        res.status(200).json(resultFromController);
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({error: "internal server error"})
    }
})

router.get('/display/worker_hiring', async (req, res) => {
    try {
        const resultFromController = await humanResourcesControllers.displayWorkerHiring();
        console.log(`Result from controller: ${resultFromController}`);

        res.status(200).json(resultFromController);
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({error: "internal server error"})
    }
})
module.exports = router;