const express = require('express');
const router = express.Router();
const userControllers = require ('../Controllers/userControllers');
const auth = require ('../auth')


router.get("/hi", (req,res) => {
    // Respond with "Hello World" for testing
    res.send("Hello World");
});

router.get("/test", (req, res) => {
    // Respond with a message indicating that the server is connected
    res.send("Server is connected!");
});

router.post('/register' , async (req, res) => {
    try {
        userControllers.registerUser(req.body)
        .then(
            resultFromController => res.send(resultFromController)
            // res.status(201).json({ message: 'User registered successfully' })
        )
        
    } catch (error) {
        console.error('Error registering user:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

router.post('/login' , async (req, res) => {
    try {
        userControllers.loginUser(req.body)
        .then(
            resultFromController => res.send(resultFromController)
        )

    } catch (err) {
        console.error ('Error Logging in: ', err);
        res.status(500).json({ err: 'Internal Server Error'})
    }
})

router.get("/details", auth.verify, (req, res) => {

    // Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
    const data = {
       userId: auth.decode(req.headers.authorization).id
    }
    console.log(data)

    // Provides the user's ID for the getProfile controller method
    userControllers.getProfile(data).then(resultFromController => res.send(resultFromController));
});



module.exports = router;