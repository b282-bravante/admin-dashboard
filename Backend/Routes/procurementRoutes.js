const express = require('express');
const router = express.Router();
const procurementControllers = require ('../Controllers/procurementControllers');


router.post ("/customs", async (req,res) => {
    try {
        const resultFromController =  await procurementControllers.saveCustomsClearance (req.body)
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})


router.get ("/display/customs", async (req,res) => {
    try {
        const resultFromController =  await procurementControllers.displayCustomsClearance ()
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})


router.post ("/purchase", async (req,res) => {
    try {
        const resultFromController =  await procurementControllers.savePurchaseClearance (req.body)
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})


router.get ("/display/purchase", async (req,res) => {
    try {
        const resultFromController =  await procurementControllers.displayPurchaseClearance ()
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})










module.exports = router;