// Routes.js
const accountingRoutes = require('./accountingRoutes');
const procurementRoutes = require('./procurementRoutes');
const generalAffairsRoutes = require('./generalAffairsRoutes')
const humanResourcesRoutes = require('./humanResourcesRoutes')

module.exports = { accountingRoutes, procurementRoutes, generalAffairsRoutes,humanResourcesRoutes };
