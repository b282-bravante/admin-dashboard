const express = require('express');
const router = express.Router();

const generalAffairsControllers = require ('../Controllers/generalAffairsControllers')


router.post ("/vehicle", async (req,res) => {
    try {
        const resultFromController =  await generalAffairsControllers.saveVehicle (req.body)
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})


router.get ("/display/vehicle", async (req,res) => {
    try {
        const resultFromController =  await generalAffairsControllers.displayVehicle ()
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})

router.post ("/korean_accoms", async (req,res) => {
    try {
        const resultFromController =  await generalAffairsControllers.saveKoreanAccoms (req.body)
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})


router.get ("/display/korean_accoms", async (req,res) => {
    try {
        const resultFromController =  await generalAffairsControllers.displayKoreanAccoms ()
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})

router.post ("/local_accoms", async (req,res) => {
    try {
        const resultFromController =  await generalAffairsControllers.saveLocalAccoms (req.body)
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})


router.get ("/display/local_accoms", async (req,res) => {
    try {
        const resultFromController =  await generalAffairsControllers.displayLocalAccoms ()
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error ("Failed to submit: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
})

router.post("/visa-nineg", async (req, res) => {
    try {
      const resultFromController = await generalAffairsControllers.saveVisaNineG(req.body);
      console.log(`Result from the controller: ${resultFromController}`);
  
      res.status(200).json(resultFromController);
    } catch (err) {
      console.error("Failed to submit: ", err);
      res.status(500).json({ error: "Internal Server Error" });
    }
  });
  
router.get("/display/visa-nineg", async (req, res) => {
    try {
      const resultFromController = await generalAffairsControllers.displayVisaNineG();
      console.log(`Result from the controller: ${resultFromController}`);
  
      res.status(200).json(resultFromController);
    } catch (err) {
      console.error("Failed to submit: ", err);
      res.status(500).json({ error: "Internal Server Error" });
    }
  });
module.exports =  router;