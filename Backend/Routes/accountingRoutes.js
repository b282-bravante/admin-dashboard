const express = require('express');
const router = express.Router();
const accountingControllers = require('../Controllers/accountingControllers');

router.get("/hi", (req, res) => {
    // Respond with "Hello World" for testing
    res.send("Hello World");
});

router.post('/tdstatus', async (req, res) => {
    try {
        // Call the asynchronous function using async/await for better error handling
        console.log(req.body);
        const resultFromController = await accountingControllers.saveTdStatus(req.body);
        // Debug point: Log the result from the controller
        console.log("Result from controller:", resultFromController);
        // Send the result back to the client
        res.send(resultFromController);
    } catch (error) {
        // Catch any errors thrown by the asynchronous function
        console.error("Failed to submit:", error);
        res.status(500).json({ error: "Internal server error" });
    }
});

router.get('/display/tdstatus', async (req, res) => {
    try {
        // Call the controller function to fetch TD status data
        const resultFromController = await accountingControllers.displayTdStatus();
        console.log("Result from controller: ", resultFromController);

        // Send the fetched data back to the client as a JSON response
        res.send(resultFromController);
    } catch (err) {
        // Handle errors
        console.error("Failed to get: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
});

router.post ('/tax', async (req, res) => {
    
    try{
        const resultFromController = await accountingControllers.saveTax(req.body);
        console.log(`Result from the controller: ${resultFromController}`);

        res.status(200).json(resultFromController)
    } catch (err) {
        console.error (`failed to submit: ${err}`);
        res.status(500).json({error: "Internal Server Error"})
    }
    
})

router.get('/display/tax', async (req, res) => {
    try {
        // Call the controller function to fetch TD status data
        const resultFromController = await accountingControllers.displayTax();
        console.log("Result from controller: ", resultFromController);

        // Send the fetched data back to the client as a JSON response
        res.status(200).json(resultFromController);
    } catch (err) {
        // Handle errors
        console.error("Failed to get: ", err);
        res.status(500).json({ error: "Internal Server Error" });
    }
});


module.exports = router;

