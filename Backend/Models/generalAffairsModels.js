const mongoose = require ('mongoose');

const vehicleSchema  = new mongoose.Schema (
    {
        dept: {
            type: String,
            required: true
        },
        plan: {
            type: String,
            required: true
        },
        actual: {
            type: String,
            required: true
        },
        remarks: {
            type: String,
            required: false
        },
    }
)


const koreanSchema  = new mongoose.Schema (
    {
        buildingNo: {
            type: String,
            required: true
        },
        total: {
            type: String,
            required: true
        },
        occupied: {
            type: String,
            required: true
        },
        remarks: {
            type: String,
            required: false
        },
    }
)

const localSchema  = new mongoose.Schema (
    {
        buildingNo: {
            type: String,
            required: true
        },
        total: {
            type: String,
            required: true
        },
        occupied: {
            type: String,
            required: true
        },
        remarks: {
            type: String,
            required: false
        },
    }
)

const visaSchema = new mongoose.Schema (
    {
        name: {
            type: String,
            required: true
        },
        arrivalDate: {
            type: String,
            required: true
        },
        status: {
            type: String,
            required: true
        },
        remarks: {
            type: String,
            required: false
        },
    }
)

const nineGSchema = new mongoose.Schema (
    {
        aepStatus: {
            type: String,
            required: true,
        },
        aepDate: {
            type: String,
            required: true,
        },
        aepRemarks: {
            type: String,
            required: true,
        },
    }
)
const Visa = mongoose.model("Visa", visaSchema);
const NineG = mongoose.model("NineG", nineGSchema);
const Vehicle = mongoose.model("Vehicle", vehicleSchema);
const KoreanAccoms = mongoose.model("KoreanAccoms", koreanSchema);
const LocalAccoms = mongoose.model("LocalAccoms", localSchema);

module.exports ={ Vehicle , KoreanAccoms, LocalAccoms, Visa, NineG}