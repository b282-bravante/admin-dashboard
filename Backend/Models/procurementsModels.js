const mongoose = require('mongoose');


const customsSchema = new mongoose.Schema({
    dept: {
        type: String,
        required: true
    },
    plan: {
        type: String,
        required: true
    },
    actual: {
        type: String,
        required: true
    },
    remarks: {
        type: String,
        required: false
    },
})

const purchaseSchema = new mongoose.Schema ({
    dept: {
        type: String,
        required: true
    },
    plan: {
        type: String,
        required: true
    },
    actual: {
        type: String,
        required: true
    },
    remarks: {
        type: String,
        required: false
    },
    
})

const Customs = mongoose.model('Customs', customsSchema);
const Purchase = mongoose.model('Purchase', purchaseSchema);

module.exports = {Customs, Purchase};