const mongoose = require('mongoose');

const taxSchema = new mongoose.Schema(
    [
        {
            category: {
                type: String,
                required: true
            },
            dueDate: {
                type: String,
                required: true
            },
            status: {
                type: String,
                required: true
            }
        }
    ]
);

const tdStatusSchema = new mongoose.Schema({
    bank: {
        type: String,
        required: true
    },
    kebData: [{
        kebUsdAmount: {
            type: String,
            required: function() {
                return this.bank === 'KEB';
            }
        },
        kebUsdDueDate: {
            type: String,
            required: function() {
                return this.bank === 'KEB';
            }
        },
        kebPhpAmount: {
            type: String,
            required: function() {
                return this.bank === 'KEB';
            }
        },
        kebPhpDueDate: {
            type: String,
            required: function() {
                return this.bank === 'KEB';
            }
        }
    }],
    bdoData: [{
        bdoUsdAmount: String,
        bdoUsdDueDate: String,
        bdoPhpAmount: String,
        bdoPhpDueDate: String
    }]
});


const Tax = mongoose.model('Tax', taxSchema);
const TdStatus = mongoose.model('TdStatus', tdStatusSchema);

module.exports = { Tax, TdStatus };
