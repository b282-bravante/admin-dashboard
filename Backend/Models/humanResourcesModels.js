const mongoose = require ("mongoose");

const contributionsSchema = new mongoose.Schema(
    [
        {
            category: {
                type: String,
                required: true
            },
            dueDate: {
                type: String,
                required: true
            },
            status: {
                type: String,
                required: true
            }
        }
    ]
);

const countSchema = new mongoose.Schema ([{
    dept: {
        type: String,
        required: true
    },
    
    count: {
        type: String,
        required: true
    },

    direct: {
        type: String,
        required: true
    },

    indirect: {
        type: String,
        required: true
    },

    staff: {
        type: String,
        required: true
    }

}])

const hiringSchema = new mongoose.Schema (
    {
        lrsNo: {
            type: String,
            required: true
        },
        recruitDate: {
            type: String,
            required: true
        },
        reqTeam: {
            type: String,
            required: true
        },
        hired: {
            type: String,
            required: true
        },
        remarks: {
            type: String,
            required: true
        }
      }
)

const WorkerHiringSchema = new mongoose.Schema (
    {
        lrsNo: {
            type: String,
            required: true
        },
        recruitDate: {
            type: String,
            required: true
        },
        reqTeam: {
            type: String,
            required: true
        },
        hired: {
            type: String,
            required: true
        },
        remarks: {
            type: String,
            required: true
        }
      }
)


const Hiring = mongoose.model ("Hiring", hiringSchema);
const WorkerHiring = mongoose.model ("WorkerHiring", WorkerHiringSchema);
const Contributions = mongoose.model("Contributions", contributionsSchema);
const Count = mongoose.model("Count", countSchema)

module.exports = {Contributions, Count, Hiring, WorkerHiring};