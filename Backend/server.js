const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require ('./Routes/userRoutes')
const {accountingRoutes, procurementRoutes, generalAffairsRoutes, humanResourcesRoutes} = require('./Routes/Routes')


const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

const mongoURI = 'mongodb://localhost:27017/AdminDB';

// Connect to MongoDB
mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('Connected to MongoDB');
}).catch(err => {
    console.error('Error connecting to MongoDB:', err);
});

app.use("/users", userRoutes);
app.use("/accounting", accountingRoutes);
app.use("/procurement", procurementRoutes);
app.use("/general_affairs", generalAffairsRoutes)
app.use("/human_resources", humanResourcesRoutes)

const PORT = 3030;
const IP_ADDRESS = '192.168.123.177';
app.listen(PORT, IP_ADDRESS, () => {
  console.log(`Server is running on port ${IP_ADDRESS}:${PORT}`);
});