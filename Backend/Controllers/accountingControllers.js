const accountingModels = require ("../Models/accountingModels.js")

module.exports.saveTdStatus = async (reqBody) => {
    try {
        const { bank, usdAmount, usdDueDate, phpAmount, phpDueDate } = reqBody;

        // Check if any of the fields are undefined
        if (bank === 'KEB' && (usdAmount === undefined || usdDueDate === undefined || phpAmount === undefined || phpDueDate === undefined)) {
            throw new Error('One or more required fields are undefined for KEB bank');
        } else if (bank === 'BDO' && (usdAmount === undefined || usdDueDate === undefined || phpAmount === undefined || phpDueDate === undefined)) {
            throw new Error('One or more required fields are undefined for BDO bank');
        }

        // Convert fields to strings
        const usdAmountStr = usdAmount.toString();
        const usdDueDateStr = usdDueDate.toString();
        const phpAmountStr = phpAmount.toString();
        const phpDueDateStr = phpDueDate.toString();

        // Find existing record for the bank
        const existingData = await accountingModels.TdStatus.findOne({ bank });

        // Append data to the existing array or create a new one
        if (existingData) {
            if (bank === 'KEB') {
                existingData.kebData.push({
                    kebUsdAmount: usdAmountStr,
                    kebUsdDueDate: usdDueDateStr,
                    kebPhpAmount: phpAmountStr,
                    kebPhpDueDate: phpDueDateStr
                });
            } else if (bank === 'BDO') {
                existingData.bdoData.push({
                    bdoUsdAmount: usdAmountStr,
                    bdoUsdDueDate: usdDueDateStr,
                    bdoPhpAmount: phpAmountStr,
                    bdoPhpDueDate: phpDueDateStr
                });
            }
            await existingData.save();
        } else {
            // Create new record if not exists
            const newData = new accountingModels.TdStatus({
                bank,
                ...(bank === 'KEB' ? { kebData: [{ kebUsdAmount: usdAmountStr, kebUsdDueDate: usdDueDateStr, kebPhpAmount: phpAmountStr, kebPhpDueDate: phpDueDateStr }] } :
                                      { bdoData: [{ bdoUsdAmount: usdAmountStr, bdoUsdDueDate: usdDueDateStr, bdoPhpAmount: phpAmountStr, bdoPhpDueDate: phpDueDateStr }] })
            });
            await newData.save();
        }

        return { success: true, message: 'TdStatus saved successfully' };
    } catch (error) {
        return { success: false, error: error.message };
    }
};

module.exports.displayTdStatus = async () => {
    try {
        // Fetch all data from the database
        const tdStatusData = await accountingModels.TdStatus.find();

        // Separate the data based on bank
        const kebData = [];
        const bdoData = [];
    tdStatusData.forEach(item => {
        if (item.kebData.length > 0) {
            kebData.push(...item.kebData); // Include all records for KEB
        }
        if (item.bdoData.length > 0) {
            bdoData.push(...item.bdoData); // Include all records for BDO
        }
    });

    // Return the separated data
    return { kebData, bdoData };
    } catch (err) {
        console.error('Something went wrong:', err);
        throw err;
    }
};

module.exports.saveTax = async (reqBody) => {
    try {
        // Iterate over each tax object in the array
        for (const taxData of reqBody) {
            const { category, dueDate, status } = taxData;

            // Find an existing Tax document based on category
            const existingTax = await accountingModels.Tax.findOne({ category });

            // If an existing Tax document is found, update its fields
            if (existingTax) {
                existingTax.dueDate = dueDate;
                existingTax.status = status;
                await existingTax.save();
            } else {
                // If no existing Tax document is found, create a new one
                const newTax = new accountingModels.Tax({
                    category,
                    dueDate,
                    status
                });
                await newTax.save();
            }
        }

        // Return success response
        return { success: true, message: 'Tax saved successfully' };
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};



module.exports.displayTax = async () => {
    try {
        // Fetch all tax data from the database
        const taxData = await accountingModels.Tax.find();
        
        // Log the tax data before returning
        console.log('Tax data:', taxData);

        // Return the tax data
        return taxData;
    } catch (err) {
        // Log the error
        console.error('Error while fetching tax data:', err);

        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
}





