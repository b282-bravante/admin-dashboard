const generalAffairsModels = require ('../Models/generalAffairsModels')
//////VEHICLES
module.exports.saveVehicle = async (reqBody) => {
    try {
        const { dept, plan, actual, remarks } = reqBody;

        // Check if the department already exists in the database
        const existingDept = await generalAffairsModels.Vehicle.findOne({ dept });

        if (existingDept) {
            // Department already exists, update the entry
            existingDept.plan = plan;
            existingDept.actual = actual;
            existingDept.remarks = remarks;
            await existingDept.save();
            return { success: true, message: 'Vehicle clearance updated successfully' };
        } else {
            // Department doesn't exist, create a new entry
            const newVehicle= new generalAffairsModels.Vehicle({
                dept,
                plan,
                actual,
                remarks
            });
            await newVehicle.save();
            return { success: true, message: 'Vehicle clearance saved successfully' };
        }
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};

module.exports.displayVehicle = async () => {
    try {
        // Fetch all VehicleClearance data from the database
        const VehicleData = await generalAffairsModels.Vehicle.find();
        
        // Log the Contributions data before returning
        console.log('Vehicle data:', VehicleData);

        // Return the Contributions data
        return VehicleData;
    } catch (err) {
        // Log the error
        console.error('Error while fetching Vehicle Data:', err);

        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
}

module.exports.saveKoreanAccoms = async (reqBody) => {
    try {
        const { buildingNo, total, occupied, remarks } = reqBody;

        // Check if the department already exists in the database
        const existingBuildingNo = await generalAffairsModels.KoreanAccoms.findOne({ buildingNo });

        if (existingBuildingNo) {
            // Department already exists, update the entry
            existingBuildingNo.total = total;
            existingBuildingNo.occupied = occupied;
            existingBuildingNo.remarks = remarks;
            await existingBuildingNo.save();
            return { success: true, message: 'KoreanAccoms clearance updated successfully' };
        } else {
            // Department doesn't exist, create a new entry
            const newKoreanAccoms= new generalAffairsModels.KoreanAccoms({
                buildingNo,
                total,
                occupied,
                remarks
            });
            await newKoreanAccoms.save();
            return { success: true, message: 'KoreanAccoms clearance saved successfully' };
        }
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};

module.exports.displayKoreanAccoms = async () => {
    try {
        // Fetch all KoreanAccomsClearance data from the database
        const KoreanAccomsData = await generalAffairsModels.KoreanAccoms.find();
        
        // Log the Contributions data before returning
        console.log('KoreanAccoms data:', KoreanAccomsData);

        // Return the Contributions data
        return KoreanAccomsData;
    } catch (err) {
        // Log the error
        console.error('Error while fetching KoreanAccoms Data:', err);

        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
}

module.exports.saveLocalAccoms = async (reqBody) => {
    try {
        const { buildingNo, total, occupied, remarks } = reqBody;

        // Check if the department already exists in the database
        const existingBuildingNo = await generalAffairsModels.LocalAccoms.findOne({ buildingNo });

        if (existingBuildingNo) {
            // Department already exists, update the entry
            existingBuildingNo.total = total;
            existingBuildingNo.occupied = occupied;
            existingBuildingNo.remarks = remarks;
            await existingBuildingNo.save();
            return { success: true, message: 'LocalAccoms clearance updated successfully' };
        } else {
            // Department doesn't exist, create a new entry
            const newLocalAccoms= new generalAffairsModels.LocalAccoms({
                buildingNo,
                total,
                occupied,
                remarks
            });
            await newLocalAccoms.save();
            return { success: true, message: 'LocalAccoms saved successfully' };
        }
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};

module.exports.displayLocalAccoms = async () => {
    try {
        // Fetch all LocalAccomsClearance data from the database
        const LocalAccomsData = await generalAffairsModels.LocalAccoms.find();
        
        // Log the Contributions data before returning
        console.log('LocalAccoms data:', LocalAccomsData);

        // Return the Contributions data
        return LocalAccomsData;
    } catch (err) {
        // Log the error
        console.error('Error while fetching LocalAccoms Data:', err);

        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
}

module.exports.saveVisaNineG = async(reqBody) => {
    try {
        const { visaData, nineGData } = reqBody;

        const newVisa = generalAffairsModels.Visa({
            name: visaData.name,
            arrivalDate: visaData.arrivalDate,
            status: visaData.status,
            remarks: visaData.remarks
        });

        const newNineG = generalAffairsModels.NineG({
            aepStatus: nineGData.aepStatus,
            aepDate: nineGData.aepDate,
            aepRemarks: nineGData.aepRemarks
        });

        await newVisa.save();
        await newNineG.save();

        return { success: true, message : 'Data save success' };
    } catch (err) {
        return { success: false, error: err.message };
    }
};

module.exports.displayVisaNineG = async () => {
    try {
        const visaData = await generalAffairsModels.Visa.find();
        const nineGData = await generalAffairsModels.NineG.find();
        
        console.log('Visa data:', visaData);
        console.log('NineG data:', nineGData);

        return { success: true, visaData, nineGData };
    } catch (err) {
        console.error('Error while fetching Visa and NineG Data:', err);
        return { success: false, error: err.message };
    }
};
