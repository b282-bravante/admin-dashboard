const humanResourcesModels = require("../Models/humanResourcesModels");

// Function to save Contributions
module.exports.saveContributions = async (reqBody) => {
    try {
        // Iterate over each Contributions object in the array
        for (const ContributionsData of reqBody) {
            const { category, dueDate, status } = ContributionsData;

            // Find an existing Contributions document based on category
            const existingContributions = await humanResourcesModels.Contributions.findOne({ category });

            // If an existing Contributions document is found, update its fields
            if (existingContributions) {
                existingContributions.dueDate = dueDate;
                existingContributions.status = status;
                await existingContributions.save();
            } else {
                // If no existing Contributions document is found, create a new one
                const newContributions = new humanResourcesModels.Contributions({
                    category,
                    dueDate,
                    status
                });
                await newContributions.save();
            }
        }

        // Return success response
        return { success: true, message: 'Contributions saved successfully' };
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};

module.exports.displayContributions = async () => {
    try {
        // Fetch all tax data from the database
        const contributionsData = await humanResourcesModels.Contributions.find();
        
        // Log the tax data before returning
        console.log('Contributions data:', contributionsData);

        // Return the tax data
        return contributionsData;
    } catch (err) {
        // Log the error
        console.error('Error while fetching tax data:', err);

        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
}

// Function to save Counts
module.exports.saveCounts = async (reqBody) => {
    try {
      // Iterate over each count data in the array
      for (const countData of reqBody) {
        const { dept, count, direct, indirect, staff } = countData;
  
        // Find an existing Count document based on department
        const existingCount = await humanResourcesModels.Count.findOne({ dept });
  
        // If an existing Count document is found, update its fields
        if (existingCount) {
          existingCount.count = count;
          existingCount.direct = direct;
          existingCount.indirect = indirect;
          existingCount.staff = staff;
          await existingCount.save();
        } else {
          // If no existing Count document is found, create a new one
          const newCount = new humanResourcesModels.Count({
            dept,
            count,
            direct,
            indirect,
            staff
          });
          await newCount.save();
        }
      }
  
      // Return success response
      return { success: true, message: 'Employee count saved successfully' };
    } catch (err) {
      // Return error response if an error occurs
      return { success: false, error: err.message };
    }
  };
  




  module.exports.displayCount = async () => {
    try {
      // Get all Count data
      const counts = await humanResourcesModels.Count.find();
  
      // Create an array to store the counts for each department
      const countsArray = counts.map(count => ({
        dept: count.dept,
        count: count.count,
        direct: count.direct,
        indirect: count.indirect,
        staff: count.staff
      }));
  
      // Return the counts array
      return { success: true, counts: countsArray };
    } catch (err) {
      // Return error response if an error occurs
      return { success: false, error: err.message };
    }
  };
  

module.exports.saveHiring = async (reqBody) => {
    try {
        // Destructure the single hiring object from reqBody
        const { lrsNo, recruitDate, reqTeam, hired, remarks } = reqBody;

        // Find an existing hiring document based on lrsNo
        const existingHiring = await humanResourcesModels.Hiring.findOne({ lrsNo });

        // If an existing hiring document is found, update its fields
        if (existingHiring) {
            existingHiring.recruitDate = recruitDate;
            existingHiring.reqTeam = reqTeam;
            existingHiring.hired = hired;
            existingHiring.remarks = remarks;
            await existingHiring.save();
        } else {
            // If no existing hiring document is found, create a new one
            const newHiring = new humanResourcesModels.Hiring({
                lrsNo,
                recruitDate,
                reqTeam,
                hired,
                remarks
            });
            await newHiring.save();
        }

        // Return success response
        return { success: true, message: 'Hiring data saved successfully' };
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};


module.exports.saveWorkerHiring = async (reqBody) => {
    try {
        // Destructure the single worker hiring object from reqBody
        const { lrsNo, recruitDate, reqTeam, hired, remarks } = reqBody;

        // Find an existing worker hiring document based on lrsNo
        const existingWorkerHiring = await humanResourcesModels.WorkerHiring.findOne({ lrsNo });

        // If an existing worker hiring document is found, update its fields
        if (existingWorkerHiring) {
            existingWorkerHiring.recruitDate = recruitDate;
            existingWorkerHiring.reqTeam = reqTeam;
            existingWorkerHiring.hired = hired;
            existingWorkerHiring.remarks = remarks;
            await existingWorkerHiring.save();
        } else {
            // If no existing worker hiring document is found, create a new one
            const newWorkerHiring = new humanResourcesModels.WorkerHiring({
                lrsNo,
                recruitDate,
                reqTeam,
                hired,
                remarks
            });
            await newWorkerHiring.save();
        }

        // Return success response
        return { success: true, message: 'Worker hiring data saved successfully' };
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};

module.exports.displayHiring = async () => {
    try {
        // Get all Hiring data
        const hirings = await humanResourcesModels.Hiring.find();

        // Create a single object to store the hirings for each lrsNo
        

        // Return the hirings object
        return {hirings};
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};

module.exports.displayWorkerHiring = async () => {
    try {
        // Get all Hiring data
        const workerHirings = await humanResourcesModels.WorkerHiring.find();

        // Create a single object to store the hirings for each lrsNo
        

        // Return the hirings object
        return {workerHirings};
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};

