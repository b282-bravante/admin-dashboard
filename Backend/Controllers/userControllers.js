const userModels = require('../Models/userModels');
const bcrypt = require ('bcrypt');
const auth = require ('../auth')


module.exports.registerUser = async (reqBody) => {
    const result_1 = await userModels.find({userName: reqBody.userName});
    if (result_1.length > 0) {
        return false;
    } else {
        let newUser = new userModels ({
            firstName: reqBody.firstName,
            lastName: reqBody.lastName,
            userName: reqBody.userName,
            password: bcrypt.hashSync(reqBody.password, 10),
            department: reqBody.department

        });

        return newUser.save().then((user,err) => {
            if (err) {
                return false;
            } else {
                return true;
            }
        });
    };
    
};

module.exports.loginUser = async (reqBody) => {
    return userModels.findOne({userName: reqBody.userName})
    .then(
        result => {
            if (result == null) {
                return false;
            } else {
                console.log(result)
                const isPasswordCorrect =  bcrypt.compareSync(reqBody.password, result.password);

                if (isPasswordCorrect){
                    const access = auth.createAccessToken(result);
                    const activeUser = result.department;
                    const userId = result.userName;
                    console.log(activeUser);
                    console.log(userId);
                    console.log(access); // Log the access token
                    return { access , activeUser }; // Return the access token in an object
                } else {
                    return false;
                }
            }
        }
    )
}

module.exports.getProfile = async (data) => {
    return userModels.findById(data.userId).then(result => {
        // Changes the value of the user's password to an empty string when returned to the frontend
        // Not doing so will expose the user's password which will also not be needed in other parts of our application
        // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
        result.password = "";
        // Returns the user information with the password as an empty string
        console.log(result)
        return result;
        });
    };