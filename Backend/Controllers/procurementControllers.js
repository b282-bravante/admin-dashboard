const procurementsModels = require ('../Models/procurementsModels');


module.exports.saveCustomsClearance = async (reqBody) => {
    try {
        const { dept, plan, actual, remarks } = reqBody;

        // Check if the department already exists in the database
        const existingDept = await procurementsModels.Customs.findOne({ dept });

        if (existingDept) {
            // Department already exists, update the entry
            existingDept.plan = plan;
            existingDept.actual = actual;
            existingDept.remarks = remarks;
            await existingDept.save();
            return { success: true, message: 'Customs clearance updated successfully' };
        } else {
            // Department doesn't exist, create a new entry
            const newCustoms= new procurementsModels.Customs({
                dept,
                plan,
                actual,
                remarks
            });
            await newCustoms.save();
            return { success: true, message: 'Customs clearance saved successfully' };
        }
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};

module.exports.displayCustomsClearance = async () => {
    try {
        // Fetch all customsClearance data from the database
        const customsData = await procurementsModels.Customs.find();
        
        // Log the tax data before returning
        console.log('Customs data:', customsData);

        // Return the tax data
        return customsData;
    } catch (err) {
        // Log the error
        console.error('Error while fetching Customs Data:', err);

        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
}

module.exports.savePurchaseClearance = async (reqBody) => {
    try {
        const { dept, plan, actual, remarks } = reqBody;

        // Check if the department already exists in the database
        const existingDept = await procurementsModels.Purchase.findOne({ dept });

        if (existingDept) {
            // Department already exists, update the entry
            existingDept.plan = plan;
            existingDept.actual = actual;
            existingDept.remarks = remarks;
            await existingDept.save();
            return { success: true, message: 'Purchase clearance updated successfully' };
        } else {
            // Department doesn't exist, create a new entry
            const newPurchase= new procurementsModels.Purchase({
                dept,
                plan,
                actual,
                remarks
            });
            await newPurchase.save();
            return { success: true, message: 'Purchase clearance saved successfully' };
        }
    } catch (err) {
        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
};

module.exports.displayPurchaseClearance = async () => {
    try {
        // Fetch all PurchaseClearance data from the database
        const purchaseData = await procurementsModels.Purchase.find();
        
        // Log the tax data before returning
        console.log('Purchase data:', purchaseData);

        // Return the tax data
        return purchaseData;
    } catch (err) {
        // Log the error
        console.error('Error while fetching Purchase Data:', err);

        // Return error response if an error occurs
        return { success: false, error: err.message };
    }
}


