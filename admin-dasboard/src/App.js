import './App.css';
import Dashboard from './Pages/Dashboard';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './Components/NavBar'; // Import your Navbar component
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Login from './Pages/Login';
import * as InputForms from './Components/InputForms/'; // Import your input forms
import Count from './Components/HumanResource/Count';
import Register from './Pages/Register';
import { useEffect, useState } from 'react';
import { UserProvider } from './UserContex'; // Assuming you have a UserContext
import Logout from './Pages/Logout';

function App() {
  const [user, setUser] = useState({
    userName: null,
    department: null,
    isAdmin: false,
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({ userName: null, department: null }); // Ensure user state is updated on logout
  };

  

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <div className="App">
          <NavBar />
          <Routes>
            <Route path="/" element={<Dashboard />} />
            <Route path="/login" element={<Login />} />
            <Route path="/tax" element={<InputForms.TaxInputForm />} />
            <Route path="/tdstatus" element={<InputForms.TdStatusInputForm />} />
            <Route path="/korean accomodations" element={<InputForms.KoreanAccomInputForm />} />
            <Route path="/local accomodations" element={<InputForms.LocalAccomInputForm />} />
            <Route path="/vehicles" element={<InputForms.VehicleInputForm />} />
            <Route path="/visa" element={<InputForms.VisaInputForm />} />
            <Route path="/employees" element={<InputForms.CountInputForm />} />
            <Route path="/contributions" element={<InputForms.ContributionInputForm />} />
            <Route path="/hiring" element={<InputForms.HiringInputForm />} />
            <Route path="/worker hiring" element={<InputForms.WorkerHiringInputForm />} />
            <Route path="/customs" element={<InputForms.CustomsClearanceInputForm />} />
            <Route path="/purchases" element={<InputForms.PurchasesInputForm />} />
            <Route path="/count" element={<Count />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
          </Routes>
        </div>
      </Router>
    </UserProvider>
  );
}

export default App;
