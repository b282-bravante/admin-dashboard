import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import AdminTeamLogo from '../Images/AdminTeamLogo.svg';
import UserContext from '../UserContex';

function NavBar() {
  const { user } = useContext(UserContext);

  const isDisabled = (department) => {
    return !(user && (user.department === department || user.isAdmin));
  };

  return (
    <div className='navbar-container'>
      <div className='navi-links'>
        <NavLink to="/">
          <img src={AdminTeamLogo} alt="Admin Team Logo" className="admin-team-logo" />
          <h1 className='banner-name'>HDEC</h1>
          
        </NavLink>
        <span className='devs'><h5>Dev: IT Team</h5></span>
      </div>

      

      <div className='navi-links pos-center'>
        {user ? (
          <>
            <div className={`dropdown ${isDisabled('Accounting') ? 'disabled' : ''}`}>
              <span className='dropdown-header'>Accounting</span>
              <div className='dropdown-content'>
                <NavLink to="/Tax">Tax</NavLink>
                <NavLink to="/TdStatus">TdStatus</NavLink>
              </div>
            </div>

            <div className={`dropdown ${isDisabled('Human Resource') ? 'disabled' : ''}`}>
              <span className='dropdown-header'>Human Resource</span>
              <div className='dropdown-content'>
                <NavLink to="/Contributions">Contributions</NavLink>
                <NavLink to="/Employees">Employees</NavLink>
                <NavLink to="/Hiring">Hiring</NavLink>
                <NavLink to="/Worker Hiring">Worker Hiring</NavLink>
              </div>
            </div>

            <div className={`dropdown ${isDisabled('General Affairs') ? 'disabled' : ''}`}>
              <span className='dropdown-header'>General Affairs</span>
              <div className='dropdown-content'>
                <NavLink to="/Local Accomodations">Local Accomodations</NavLink>
                <NavLink to="/Korean Accomodations">Korean Accomodations</NavLink>
                <NavLink to="/Vehicles">Vehicles</NavLink>
                <NavLink to="/VISA">VISA</NavLink>
              </div>
            </div>

            <div className={`dropdown ${isDisabled('Procurement') ? 'disabled' : ''}`}>
              <span className='dropdown-header'>Procurements</span>
              <div className='dropdown-content'>
                <NavLink to="/Customs">Customs Clearance</NavLink>
                <NavLink to="/Purchases">Purchases</NavLink>
              </div>
            </div>
          </>
        ) : null}
      </div>

      <div className='navi-links'>
        {user && user.department !== null ? (
          <>
            <NavLink className='mx-1' to="/logout">Logout</NavLink>
            {user.isAdmin ? (
              <NavLink className='mx-1' to="/register">Register</NavLink>
            ) : null}
          </>
        ) : (
          <>
            <NavLink className='mx-1' to="/login">Login</NavLink>
            
          </>
        )}
      </div>
    </div>
  );
}

export default NavBar;
