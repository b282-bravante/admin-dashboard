import React, { useEffect, useState } from 'react';
import { Card, Table } from 'react-bootstrap';

function Hiring() {
  // State to hold the data
  const [hiringData, setHiringData] = useState([]);

  useEffect(() => {
    // Fetch data from backend when component mounts
    fetchHiringData();
  }, []);

  // Function to fetch hiring data from backend
  const fetchHiringData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/human_resources/display/hiring`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        console.log('Hiring data fetched successfully:', data);
        // Set the hiring data in state
        setHiringData(data.hirings);
      })
      .catch(error => {
        console.error('Error fetching hiring data:', error);
      });
  };

  return (
    <Card className="text-center card-wrapper" bg="primary" text="light">
      
      <Card.Body>
        <Card.Title><h1>Hiring Status</h1></Card.Title>
        <Card.Text>
          <table className='custom-table'>
            <thead>
              <tr>
                <th>LRS No.</th>
                <th>Recruit Date</th>
                <th>Req Team</th>
                <th>HIRED</th>
                <th>Remarks</th>
              </tr>
            </thead>
            <tbody>
              {/* Map through the hiring data to display table rows */}
              {hiringData.map((item, index) => (
                <tr key={index}>
                  <td>{item.lrsNo}</td>
                  <td>{item.recruitDate}</td>
                  <td>{item.reqTeam}</td>
                  <td>{item.hired}</td>
                  <td>{item.remarks}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Card.Text>
      </Card.Body>
      <Card.Footer className="text-muted"></Card.Footer>
    </Card>
  );
}

export default Hiring;
