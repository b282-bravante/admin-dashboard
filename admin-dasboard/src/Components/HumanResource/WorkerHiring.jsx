import React, { useEffect, useState } from 'react';
import { Card, Table } from 'react-bootstrap';

function WorkerHiring() {
  // State to hold the data
  const [workerHiringData, setWorkerHiringData] = useState([]);

  useEffect(() => {
    // Fetch data from backend when component mounts
    fetchWorkerHiringData();
  }, []);

  // Function to fetch worker hiring data from backend
  const fetchWorkerHiringData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/human_resources/display/worker_hiring`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        console.log('Worker Hiring data fetched successfully:', data);
        // Set the worker hiring data in state
        setWorkerHiringData(data.workerHirings);
      })
      .catch(error => {
        console.error('Error fetching worker hiring data:', error);
      });
  };

  return (
    <Card className="text-center card-wrapper" bg="primary" text="light">
      
      <Card.Body>
        <Card.Title><h1>Worker Hiring Status</h1></Card.Title>
        <Card.Text>
          <table className='custom-table'>
            <thead>
              <tr>
                <th>LRS No.</th>
                <th>Recruit Date</th>
                <th>Req Team</th>
                <th>Hired</th>
                <th>Remarks</th>
              </tr>
            </thead>
            <tbody>
              {/* Map through the worker hiring data to display table rows */}
              {workerHiringData.map((item, index) => (
                <tr key={index}>
                  <td>{item.lrsNo}</td>
                  <td>{item.recruitDate}</td>
                  <td>{item.reqTeam}</td>
                  <td>{item.hired}</td>
                  <td>{item.remarks}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Card.Text>
      </Card.Body>
      <Card.Footer className="text-muted"></Card.Footer>
    </Card>
  );
}

export default WorkerHiring;
