import React, { useState, useEffect } from 'react';
import { Card, Table } from 'react-bootstrap';

function Contributions() {
  const [data, setData] = useState([]);
  

  // For now, retrieve data from localStorage
  useEffect(() => {
    fetchTdContributionsData();
  }, []);

  const fetchTdContributionsData = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/human_resources/display/contributions`);
      if (response.ok) {
        const data = await response.json();
        setData(data);
      } else {
        console.error('Failed to fetch data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  return (
    <Card className="text-center card-wrapper" bg='primary' text='light'>
      
      <Card.Body>
        <Card.Title><h1>Contributions</h1></Card.Title>
        <Card.Text>
          <table className='custom-table'>
            <thead>
              <tr>
                <th>Name</th>
                <th>Due Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {data.map((item, index) => (
                <tr key={index}>
                  <td>{item.category}</td>
                  <td>{item.dueDate}</td>
                  <td>{item.status}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Card.Text>
      </Card.Body>
      <Card.Footer className="text-muted"></Card.Footer>
    </Card>
  );
}

export default Contributions;
