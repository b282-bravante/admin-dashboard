import React, { useEffect, useState } from 'react';
import { Card, Container, Table } from 'react-bootstrap';

function Count() {
  // State to hold the data
  const [data, setData] = useState([]);

  useEffect(() => {
    // Fetch data from backend when component mounts
    fetchCountData();
  }, []);

  // Function to fetch count data from backend
  const fetchCountData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/human_resources/display/count`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        console.log('Count data fetched successfully:', data);
        setData(data.counts || []); // Update state with fetched data
      })
      .catch(error => {
        console.error('Error fetching count data:', error);
      });
  };

  // Calculate total number of employees
  const totalEmployees = Array.isArray(data)
    ? data.reduce((total, item) => total + (parseInt(item.count) || 0), 0)
    : 0;
  
  const totalDirect = Array.isArray(data)
  ? data.reduce((total, item) => total + (parseInt(item.direct) || 0), 0)
  : 0;

const totalIndirect = Array.isArray(data)
  ? data.reduce((total, item) => total + (parseInt(item.indirect) || 0), 0)
  : 0;

const totalStaff = Array.isArray(data)
  ? data.reduce((total, item) => total + (parseInt(item.staff) || 0), 0)
  : 0;


  return (
    <Card className="text-center card-wrapper" bg='primary' text='light'>
      <Card.Body>
        <Card.Title >
        <div className='flex-count'>
          <div className='flex-count manpower-wrapper'>            
            <div>
              <h5>Breakdown</h5>
              <div className='breakdown'>
                <div>
                  <h3 style={{fontWeight: 'bold'}}>Direct</h3>
                  <h3 style={{fontWeight: 'bold'}}>{totalDirect}</h3>
                </div>
                <div>
                  <h3 style={{fontWeight: 'bold'}}>Indirect</h3>
                  <h3 style={{fontWeight: 'bold'}}>{totalIndirect}</h3>
                </div>
                <div>
                  <h3 style={{fontWeight: 'bold'}}>Staff</h3>
                  <h3 style={{fontWeight: 'bold'}}>{totalStaff}</h3>
                </div> 
              </div>
            </div>
            <div>
              <h1 className='display-4' style={{fontWeight: 'bold'}}>{totalEmployees}</h1>
              <h5>Total Manpower</h5>
            </div>
          </div>
        </div>
        </Card.Title>
        <Card.Text className='card-text-wrapper'>
          <Container>
            <table className='custom-table'>
              <thead>
                <tr>
                  <th>Department</th>
                  <th>Total Count</th>
                  <th>Direct</th>
                  <th>Indirect</th>
                  <th>Staff</th>
                </tr>
              </thead>
              <tbody>
                {/* Iterate through data to display categories and values */}
                {Array.isArray(data) && data.map((item, index) => (
                  <tr key={index}>
                    <td>{item.dept}</td>
                    <td>{item.count}</td>
                    <td>{item.direct}</td>
                    <td>{item.indirect}</td>
                    <td>{item.staff}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </Container>
        </Card.Text>
      </Card.Body>
      <Card.Footer className="text-muted"></Card.Footer>
    </Card>
  );
}

export default Count;
