import React, { useState } from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';

function LocalAccomInputForm() {
  const [formData, setFormData] = useState({
    buildingNo: '',
    total: '',
    occupied: '',
    remarks: '',
    status: ''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    
    
  
    // Send a POST request to the backend
    fetch(`${process.env.REACT_APP_API_URL}/general_affairs/local_accoms`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        console.log('Success:', data);
        alert('Form submitted successfully');
        // Optionally handle success response from backend
      })
      .catch(error => {
        console.error('Error:', error);
        alert('error');
        // Optionally handle error
      });

      setFormData({
        buildingNo: '',
        total: '',
        occupied: '',
        remarks: '',
      });
  };

  // Options for building numbers
  const buildingOptions = ['0001', '0002', '0003', '0004', '0005'];

  return (
    <div className='login-container flex'>
      <div className='login-card'>         
        <h1>Local Accommodations</h1>
        <div className='input-form'>
          <Form>
            <InputGroup className='mb-3'>
              <InputGroup.Text>Building No.</InputGroup.Text>
              <Form.Select
                name='buildingNo'
                value={formData.buildingNo}
                onChange={handleChange}
              >
                <option value=''>Select Building No.</option>
                {buildingOptions.map((option, index) => (
                  <option key={index} value={option}>{option}</option>
                ))}
              </Form.Select>
            </InputGroup>
            <InputGroup className='mb-3'>
              <InputGroup.Text>Total</InputGroup.Text>
              <Form.Control
                type='number'
                name='total'
                value={formData.total}
                onChange={handleChange}
              />
            </InputGroup>
            <InputGroup className='mb-3'>
              <InputGroup.Text>Occupied</InputGroup.Text>
              <Form.Control
                type='number'
                name='occupied'
                value={formData.occupied}
                onChange={handleChange}
              />
            </InputGroup>
            <InputGroup className='mb-3'>
              <InputGroup.Text>Remarks</InputGroup.Text>
              <Form.Control
                type='text'
                name='remarks'
                value={formData.remarks}
                onChange={handleChange}
              />
            </InputGroup>
            <Button variant='primary' onClick={handleSubmit} disabled={!formData.buildingNo}>Submit</Button>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default LocalAccomInputForm;
