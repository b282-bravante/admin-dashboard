import React, { useState } from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';

function WorkerHiringInputForm() {
  const [formData, setFormData] = useState({
    lrsNo: '',
    recruitDate: '',
    reqTeam: '',
    hired: '',
    remarks: ''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = () => {
    console.log('Submit button clicked'); // Check if handleSubmit is being called
    // Send form data to backend
    sendFormDataToBackend(formData);
  };

  const sendFormDataToBackend = (formData) => {
    fetch(`${process.env.REACT_APP_API_URL}/human_resources/worker_hiring`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => {
      console.log('Form data sent to backend successfully:', data);
      // You can handle the response from the backend here
    })
    .catch(error => {
      console.error('Error sending form data to backend:', error);
    });
  };

  return (
    <div className='login-container flex'>
      <div className='login-card'>
         
        <h1>WORKER HIRING</h1>
        <div className='input-form'>
          <Form>
            <InputGroup className='mb-3'>
              <InputGroup.Text>LRS No.</InputGroup.Text>
              <Form.Control
                type='text'
                name='lrsNo'
                value={formData.lrsNo}
                onChange={handleChange}
              />
            </InputGroup>
            <InputGroup className='mb-3'>
              <InputGroup.Text>Recruit Date</InputGroup.Text>
              <Form.Control
                type='date'
                name='recruitDate'
                value={formData.recruitDate}
                onChange={handleChange}
              />
            </InputGroup>
            <InputGroup className='mb-3'>
              <InputGroup.Text>Req Team</InputGroup.Text>
              <Form.Control
                type='text'
                name='reqTeam'
                value={formData.reqTeam}
                onChange={handleChange}
              />
            </InputGroup>
            <InputGroup className='mb-3'>
              <InputGroup.Text>Hired</InputGroup.Text>
              <Form.Control
                type='text'
                name='hired'
                value={formData.hired}
                onChange={handleChange}
              />
            </InputGroup>
            <InputGroup className='mb-3'>
              <InputGroup.Text>Remarks</InputGroup.Text>
              <Form.Control
                type='text'
                name='remarks'
                value={formData.remarks}
                onChange={handleChange}
              />
            </InputGroup>
            <Button variant='primary' onClick={handleSubmit}>Submit</Button>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default WorkerHiringInputForm;
