import { useState } from 'react';
import { Form, InputGroup, Button } from 'react-bootstrap';

function CountInputForm() {
  const deptOptions = [
    "ADMIN",
    "PCM",
    "DESIGN",
    "QA/QC",
    "HSE",
    "CNSTR",
    "SURVEY",
    "BOREDPILE"
  ];

  const initialFormData = deptOptions.map(dept => ({
    dept,
    count: "",
    direct: "",
    indirect: "",
    staff: ""
  }));

  const [formData, setFormData] = useState(initialFormData);

  const handleChange = (e, index) => {
    const { name, value } = e.target;
    const updatedFormData = [...formData];
    updatedFormData[index][name] = value;
    setFormData(updatedFormData);
  };

  const handleSubmit = () => {
    // Parse and calculate counts before storing and sending
    const parsedData = formData.map(item => {
      const direct = parseInt(item.direct) || 0;
      const indirect = parseInt(item.indirect) || 0;
      const staff = parseInt(item.staff) || 0;
      const count = direct + indirect + staff;

      return {
        ...item,
        count,
        direct,
        indirect,
        staff
      };
    });

    // Store the form data in local storage
    storeFormData(parsedData);
    // Send the form data to the backend
    sendFormDataToBackend(parsedData);
  };

  const storeFormData = (data) => {
    localStorage.setItem('formData', JSON.stringify(data));
  };

  const sendFormDataToBackend = (formData) => {
    fetch(`${process.env.REACT_APP_API_URL}/human_resources/count`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => {
      console.log('Form data sent to backend successfully:', data);
      // You can handle the response from the backend here
    })
    .catch(error => {
      console.error('Error sending form data to backend:', error);
    });
  };

  return (
    <div className='login-container flex'>
      <div className='login-card'>
        <h1>EMPLOYEES</h1>
        <div className='input-form'>
          <Form>
            {formData.map((item, index) => (
              <div key={index}>
                <InputGroup className='mb-3'>
                  <InputGroup.Text id={`inputGroup-sizing-default-${index}`}>{item.dept}</InputGroup.Text>

                  <Form.Control
                    type='text'
                    name='direct'
                    placeholder='Direct'
                    value={item.direct}
                    onChange={(e) => handleChange(e, index)}
                    aria-label={`Direct for ${item.dept}`}
                    aria-describedby={`inputGroup-sizing-default-${index}`}
                  />
                  <Form.Control
                    type='text'
                    name='indirect'
                    placeholder='Indirect'
                    value={item.indirect}
                    onChange={(e) => handleChange(e, index)}
                    aria-label={`Indirect for ${item.dept}`}
                    aria-describedby={`inputGroup-sizing-default-${index}`}
                  />
                  <Form.Control
                    type='text'
                    name='staff'
                    placeholder='Staff'
                    value={item.staff}
                    onChange={(e) => handleChange(e, index)}
                    aria-label={`Staff for ${item.dept}`}
                    aria-describedby={`inputGroup-sizing-default-${index}`}
                  />
                </InputGroup>
              </div>
            ))}
            <Button variant='primary' onClick={handleSubmit}>Submit</Button>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default CountInputForm;
