import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';

function TdStatusInputForm() {
    const [kebFormData, setKebFormData] = useState({
        kebUsdAmount: '',
        kebUsdDueDate: '',
        kebPhpAmount: '',
        kebPhpDueDate: '',
    });

    const [bdoFormData, setBdoFormData] = useState({
        bdoUsdAmount: '',
        bdoUsdDueDate: '',
        bdoPhpAmount: '',
        bdoPhpDueDate: '',
    });

    const [selectedBank, setSelectedBank] = useState('KEB');

    const handleBankChange = (e) => {
        setSelectedBank(e.target.value);
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        if (selectedBank === 'KEB') {
            setKebFormData({ ...kebFormData, [name]: value });
        } else {
            setBdoFormData({ ...bdoFormData, [name]: value });
        }
    };

    const handleSubmit = () => {
        // Combine form data
        const formData = {
            bank: selectedBank,
            usdAmount: selectedBank === 'KEB' ? kebFormData.kebUsdAmount : bdoFormData.bdoUsdAmount,
            usdDueDate: selectedBank === 'KEB' ? kebFormData.kebUsdDueDate : bdoFormData.bdoUsdDueDate,
            phpAmount: selectedBank === 'KEB' ? kebFormData.kebPhpAmount : bdoFormData.bdoPhpAmount,
            phpDueDate: selectedBank === 'KEB' ? kebFormData.kebPhpDueDate : bdoFormData.bdoPhpDueDate,
        };
    
        // Sample POST request to update backend (commented out for testing purposes)
        fetch(`${process.env.REACT_APP_API_URL}/accounting/tdstatus`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData),
        })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            // Optionally handle success response
        })
        .catch((error) => {
            console.error('Error:', error);
            // Optionally handle error
        });
    
        // Update localStorage
        let storedData = JSON.parse(localStorage.getItem(selectedBank === 'KEB' ? 'tdStatusKebData' : 'tdStatusBdoData')) || [];
        storedData.push(formData);
        localStorage.setItem(selectedBank === 'KEB' ? 'tdStatusKebData' : 'tdStatusBdoData', JSON.stringify(storedData));
    
        // Reset form data
        if (selectedBank === 'KEB') {
            setKebFormData({
                kebUsdAmount: '',
                kebUsdDueDate: '',
                kebPhpAmount: '',
                kebPhpDueDate: '',
            });
        } else {
            setBdoFormData({
                bdoUsdAmount: '',
                bdoUsdDueDate: '',
                bdoPhpAmount: '',
                bdoPhpDueDate: '',
            });
        }
    };

    return (
        <div className='login-container flex'>
            <div className='login-card'>
                 
                <h1>TD Status</h1>
                <div className='input-form'>
                    <Form>
                        <Form.Group>
                            <div className='radio-options'>
                                <Form.Check
                                    type='radio'
                                    id='kebRadio'
                                    name='bankSelection'
                                    label='KEB'
                                    value='KEB'
                                    checked={selectedBank === 'KEB'}
                                    onChange={handleBankChange}
                                />
                                <Form.Check
                                    type='radio'
                                    id='bdoRadio'
                                    name='bankSelection'
                                    label='BDO'
                                    value='BDO'
                                    checked={selectedBank === 'BDO'}
                                    onChange={handleBankChange}
                                />
                            </div>
                        </Form.Group>
                        <Form.Group controlId="usdAmount">
                            <Form.Label>USD Amount</Form.Label>
                            <Form.Control
                                type='text'
                                name={selectedBank === 'KEB' ? 'kebUsdAmount' : 'bdoUsdAmount'}
                                value={selectedBank === 'KEB' ? kebFormData.kebUsdAmount : bdoFormData.bdoUsdAmount}
                                onChange={handleChange}
                            />
                        </Form.Group>
                        <Form.Group controlId="usdDueDate">
                            <Form.Label>USD Due Date</Form.Label>
                            <Form.Control
                                type='date'
                                name={selectedBank === 'KEB' ? 'kebUsdDueDate' : 'bdoUsdDueDate'}
                                value={selectedBank === 'KEB' ? kebFormData.kebUsdDueDate : bdoFormData.bdoUsdDueDate}
                                onChange={handleChange}
                            />
                        </Form.Group>
                        <Form.Group controlId="phpAmount">
                            <Form.Label>PHP Amount</Form.Label>
                            <Form.Control
                                type='text'
                                name={selectedBank === 'KEB' ? 'kebPhpAmount' : 'bdoPhpAmount'}
                                value={selectedBank === 'KEB' ? kebFormData.kebPhpAmount : bdoFormData.bdoPhpAmount}
                                onChange={handleChange}
                            />
                        </Form.Group>
                        <Form.Group controlId="phpDueDate">
                            <Form.Label>PHP Due Date</Form.Label>
                            <Form.Control
                                type='date'
                                name={selectedBank === 'KEB' ? 'kebPhpDueDate' : 'bdoPhpDueDate'}
                                value={selectedBank === 'KEB' ? kebFormData.kebPhpDueDate : bdoFormData.bdoPhpDueDate}
                                onChange={handleChange}
                            />
                        </Form.Group>
                        <Button className="btn-space" variant='primary' onClick={handleSubmit}>Submit</Button>
                    </Form>
                </div>
            </div>
        </div>
    );
}

export default TdStatusInputForm;
