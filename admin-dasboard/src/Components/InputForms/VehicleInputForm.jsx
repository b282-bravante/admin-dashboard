import React, { useState } from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';

function VehicleInputForm() {
  const [formData, setFormData] = useState({
    dept: '',
    plan: '',
    actual: '',
    remarks: '',
  });

  const handleDataChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    // Save form data to local storage
    const storedData = JSON.parse(localStorage.getItem('purchaseData')) || [];
    const existingIndex = storedData.findIndex((item) => item.dept === formData.dept);
  
    if (existingIndex !== -1) {
      // Update existing department data
      storedData[existingIndex] = formData;
    } else {
      // Add new department data
      storedData.push(formData);
    }
  
    localStorage.setItem('purchaseData', JSON.stringify(storedData));
  
    // Reset form data
    setFormData({
      dept: '',
      plan: '',
      actual: '',
      remarks: '',
    });
  
    // Optional: Display a success message or perform any other action after submission
    alert('Form submitted successfully');
  
    // Send a POST request to the backend
    fetch(`${process.env.REACT_APP_API_URL}/general_affairs/vehicle`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        console.log('Success:', data);
        // Optionally handle success response from backend
      })
      .catch(error => {
        console.error('Error:', error);
        // Optionally handle error
      });
  };
  

  // Department options
  const departmentOptions = ['ADMIN', 'PCM', 'HSE', 'CONSTRUCTION', 'DESIGN', 'SURVEY'];

  return (
    <>
      <div className='login-container flex'>
        <div className='login-card'>
           
          <h1>VEHICLES</h1>
          <div className='input-form'>
            <Form>
              <Form.Group className='mb-3' controlId='dept'>
                <Form.Label>Department</Form.Label>
                <Form.Select name='dept' value={formData.dept} onChange={handleDataChange}>
                  <option value=''>Select Department</option>
                  {departmentOptions.map((dept, index) => (
                    <option key={index} value={dept}>{dept}</option>
                  ))}
                </Form.Select>
              </Form.Group>
              <Form.Group className='mb-3' controlId='plan'>
                <Form.Label>Plan</Form.Label>
                <Form.Control type='number' name='plan' value={formData.plan} onChange={handleDataChange} />
              </Form.Group>
              <Form.Group className='mb-3' controlId='actual'>
                <Form.Label>Actual</Form.Label>
                <Form.Control type='number' name='actual' value={formData.actual} onChange={handleDataChange} />
              </Form.Group>
              <Form.Group className='mb-3' controlId='remarks'>
                <Form.Label>Remarks</Form.Label>
                <Form.Control type='text' name='remarks' value={formData.remarks} onChange={handleDataChange} />
              </Form.Group>
              <Button variant='primary' className='button-submit' onClick={handleSubmit}>
                Submit
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </>
  );
}

export default VehicleInputForm;
