import React, { useState } from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';

function VisaInputForm() {
  const [handleChangeData, setHandleChangeData] = useState({
    name: '',
    arrivalDate: '',
    status: '',
    remarks: '',
  });

  const [handleChangeNineG, setHandleChangeNineG] = useState({
    aepStatus: '',
    aepDate: '',
    aepRemarks: '',
  });

  const handleDataChange = (e) => {
    const { name, value } = e.target;
    setHandleChangeData({
      ...handleChangeData,
      [name]: value,
    });
  };

  const handleNineGChange = (e) => {
    const { name, value } = e.target;
    setHandleChangeNineG({
      ...handleChangeNineG,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    const requestData = {
      visaData: handleChangeData,
      nineGData: handleChangeNineG
    };
  
    fetch (`${process.env.REACT_APP_API_URL}/general_affairs/visa-nineg`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => {
      console.log('Success:', data);
      alert('Data submitted successfully');
      // Optionally handle success response from backend
    })
    .catch(error => {
      console.error('Error:', error);
      alert('Error submitting data');
      // Optionally handle error
    });
  
    // Reset form data
    setHandleChangeData({
      name: '',
      arrivalDate: '',
      status: '',
      remarks: '',
    });
  
    // Reset nineGData
    setHandleChangeNineG({
      aepStatus: '',
      aepDate: '',
      aepRemarks: '',
    });
  };
  

  return (
    <>
      <div className='login-container flex'>
        <div className='login-card'>
           
          <h1>Visa</h1>
          <div className='input-form'>
            <Form>
              <Form.Group className='mb-3' controlId='name'>
                <Form.Label>Name</Form.Label>
                <Form.Control type='text' name='name' value={handleChangeData.name} onChange={handleDataChange} />
              </Form.Group>
              <Form.Group className='mb-3' controlId='arrivalDate'>
                <Form.Label>Arrival Date</Form.Label>
                <Form.Control type='date' name='arrivalDate' value={handleChangeData.arrivalDate} onChange={handleDataChange} />
              </Form.Group>
              <InputGroup className='flex-row'>
                <Form.Group className='mb-3' controlId='nineG_aepStatus'>
                  <Form.Label>AEP Status</Form.Label>
                  <Form.Control
                    as='select'
                    name='aepStatus'
                    value={handleChangeNineG.aepStatus}
                    onChange={handleNineGChange}
                  >
                    <option value=''>Select AEP Status</option>
                    <option value='O'>O</option>
                    <option value='X'>X</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group className='mb-3' controlId='nineG_aepDate'>
                  <Form.Label>AEP Date</Form.Label>
                  <Form.Control
                    type='date'
                    name='aepDate'
                    value={handleChangeNineG.aepDate}
                    onChange={handleNineGChange}
                  />
                </Form.Group>
                <Form.Group className='mb-3' controlId='nineG_aepRemarks'>
                  <Form.Label>AEP Remarks</Form.Label>
                  <Form.Control
                    type='text'
                    name='aepRemarks'
                    value={handleChangeNineG.aepRemarks}
                    onChange={handleNineGChange}
                  />
                </Form.Group>
              </InputGroup>
              <InputGroup className='flex-row'>
                <Form.Group className='mb-3' controlId='status'>
                  <Form.Label>Status</Form.Label>
                  <Form.Control type='text' name='status' value={handleChangeData.status} onChange={handleDataChange} />
                </Form.Group>
                <Form.Group className='mb-3' controlId='remarks'>
                  <Form.Label>Remarks</Form.Label>
                  <Form.Control type='text' name='remarks' value={handleChangeData.remarks} onChange={handleDataChange} />
                </Form.Group>
              </InputGroup>
              <Button variant='primary' className='button-submit' onClick={handleSubmit}>
                Submit
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </>
  );
}

export default VisaInputForm;
