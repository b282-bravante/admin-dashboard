import React, { useState } from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';

function TaxInputForm() {
  const [tax, setTax] = useState([
    { category: 'WTH', dueDate: '', status: '' },
    { category: 'PAG-IBIG', dueDate: '', status: '' },
    { category: 'PHILHEALTH', dueDate: '', status: '' },
  ]);

  const handleSubmit = () => {
    // Save the values to localStorage
    localStorage.setItem('taxData', JSON.stringify(tax));

    // Console log the tax data before sending the request
    console.log('Tax data:', tax);

    // Uncomment the following lines to send a POST request to the backend
    fetch(`${process.env.REACT_APP_API_URL}/accounting/tax`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(tax),
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        console.log('Success:', JSON.stringify(data));
        // Optionally handle success response from backend
      })
      .catch(error => {
        console.error('Error:', error);
        // Optionally handle error
      });

    // Optional: Clear form fields after submission
    setTax(prevState =>
      prevState.map(contribution => ({ ...contribution, dueDate: '', status: '' }))
    );
  };

  const handleInputChange = (index, field, value) => {
    setTax(prevTax => {
      const updatedTax = [...prevTax];
      updatedTax[index] = {
        ...updatedTax[index],
        [field]: value,
      };
      return updatedTax;
    });
  };
  
  return (
    <>
      <div className='login-container flex'>
        <div className='login-card'>
           
          <h1>Tax</h1>
          <div className='input-form'>
            {/* Map through Tax array to render input fields for each category */}
            {tax.map((contribution, index) => (
              <div key={index}>
                <h4>{contribution.category}</h4>
                <InputGroup className='mb-3'>
                  <InputGroup.Text id='inputGroup-sizing-default'>Due Date</InputGroup.Text>
                  <Form.Control
                    aria-label='Due Date'
                    aria-describedby='inputGroup-sizing-default'
                    placeholder='YYYY.MM.DD'
                    value={contribution.dueDate || ''}
                    onChange={(e) => handleInputChange(index, 'dueDate', e.target.value)}
                  />
                </InputGroup>
                <InputGroup className='mb-3'>
                  <InputGroup.Text id='inputGroup-sizing-default'>Status</InputGroup.Text>
                  <Form.Select
                    aria-label='Status'
                    aria-describedby='inputGroup-sizing-default'
                    value={contribution.status || ''}
                    onChange={(e) => handleInputChange(index, 'status', e.target.value)}
                  >
                    <option value=''>Select Status</option>
                    <option value='ONGOING'>Ongoing</option>
                    <option value='EXPIRED'>Expired</option>
                  </Form.Select>
                </InputGroup>
              </div>
            ))}
            <Button variant='primary' className='button-submit' onClick={handleSubmit}>
              Submit
            </Button>
          </div>
        </div>
      </div>
    </>
  );
}

export default TaxInputForm;
