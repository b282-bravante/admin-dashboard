import React, { useEffect, useState } from 'react';
import { Card, Table } from 'react-bootstrap';

function TdStatus() {
  const [tdStatusData, setTdStatusData] = useState({ kebData: [], bdoData: [] });

  useEffect(() => {
    fetchTdStatusData();
  }, []);

  const fetchTdStatusData = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/accounting/display/tdstatus`);
      if (response.ok) {
        const data = await response.json();
        setTdStatusData(data);
      } else {
        console.error('Failed to fetch data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  return (
    <>
      <Card className="text-center card-wrapper" bg="primary" text="light">
        
        <Card.Body>
          <Card.Title><h1>TD STATUS</h1></Card.Title>
          <Card.Text>
            {(tdStatusData.kebData.length > 0 || tdStatusData.bdoData.length > 0) ? (
              <div className='flex-table'>
                {tdStatusData.kebData.length > 0 && (
                  <table  className='custom-table'>
                    <thead>
                      <tr>
                        <th colSpan={4}>KEB</th>
                      </tr>
                      <tr>
                        <th colSpan={2}>USD</th>
                        <th colSpan={2}>PHP</th>
                      </tr>
                      <tr>
                        <th>Amount</th>
                        <th>Due Date</th>
                        <th>Amount</th>
                        <th>Due Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      {tdStatusData.kebData.map((item, index) => (
                        <tr key={`keb-${index}`}>
                          <td>{item.kebUsdAmount}</td>
                          <td>{item.kebUsdDueDate}</td>
                          <td>{item.kebPhpAmount}</td>
                          <td>{item.kebPhpDueDate}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                )}
                {tdStatusData.bdoData.length > 0 && (
                  <table  className='custom-table'>
                    <thead>
                      <tr>
                        <th colSpan={4}>BDO</th>
                      </tr>
                      <tr>
                        <th colSpan={2}>USD</th>
                        <th colSpan={2}>PHP</th>
                      </tr>
                      <tr>
                        <th>Amount</th>
                        <th>Due Date</th>
                        <th>Amount</th>
                        <th>Due Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      {tdStatusData.bdoData.map((item, index) => (
                        <tr key={`bdo-${index}`}>
                          <td>{item.bdoUsdAmount}</td>
                          <td>{item.bdoUsdDueDate}</td>
                          <td>{item.bdoPhpAmount}</td>
                          <td>{item.bdoPhpDueDate}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                )}
              </div>
            ) : (
              <p>No data available</p>
            )}
          </Card.Text>
        </Card.Body>
        <Card.Footer className="text-muted"></Card.Footer>
      </Card>
    </>
  );
}

export default TdStatus;
