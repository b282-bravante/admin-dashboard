import React, { useEffect, useState } from 'react';
import { Card, Table } from 'react-bootstrap';

function LocalAccom() {
  const [localAccomData, setLocalAccomData] = useState([]);

  useEffect(() => {
    fetchLocalAccomData(); 
  }, []);

  const fetchLocalAccomData = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/general_affairs/display/local_accoms`);
      if (response.ok) {
        const data = await response.json();
        setLocalAccomData(data);
      } else {
        console.error('Failed to fetch data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const calculateDiff = (total, occupied) => total - occupied;

  const getStatus = (diff) => {
    if (diff === 0) {
      return 'FULL';
    } else if (diff > 0) {
      return 'VACANCY';
    } else {
      return 'OVER';
    }
  };

  return (
    <>
      <Card className="text-center card-wrapper" bg='primary' text='light'>
        
        <Card.Body>
          <Card.Title>
            <h1>Local Accommodation</h1>
          </Card.Title>
          <Card.Text>
            <table className='custom-table'>
              <thead>
                <tr>
                  <th>BUILDING NO.</th>
                  <th>Total</th>
                  <th>Occupied</th>
                  <th>Slot</th>
                  <th>Vacant</th>
                  <th>Remarks</th>
                </tr>
              </thead>
              <tbody>
                {localAccomData.map((building, index) => (
                  <tr key={index}>
                    <td>{building.buildingNo}</td>
                    <td>{building.total}</td>
                    <td>{building.occupied}</td>
                    <td>{calculateDiff(building.total, building.occupied)}</td>
                    <td>{getStatus(calculateDiff(building.total, building.occupied))}</td>
                    <td>{building.remarks}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </Card.Text>
        </Card.Body>
        <Card.Footer className="text-muted"></Card.Footer>
      </Card>
    </>
  );
}

export default LocalAccom;
