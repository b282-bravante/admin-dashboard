import React, { useEffect, useState } from 'react';
import { Card, Table } from 'react-bootstrap';

function Visa() {
  const [visaData,setVisaData] = useState([]);
  const [nineGData, setNineGData] = useState([])
  // Fetch data from local storage
  useEffect(()=> {
    fetchVisaData();
  },[])

  const fetchVisaData = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/general_affairs/display/visa-nineg`);
      if (response.ok) {
        const { visaData, nineGData } = await response.json();
        setVisaData(visaData);
        setNineGData(nineGData);
      } else {
        console.error(`Failed to fetch data: ${response.statusText}`);
      }
    } catch (err) {
      console.error(`There was a problem getting the data: ${err}`);
    }
  };
  
  

  return (
    <>
      <Card className='text-center card-wrapper' bg='primary' text='light'>
        
        <Card.Body>
          <Card.Title><h1>Visa</h1></Card.Title>
          <Card.Text>
            <table className='custom-table'>
              <thead>
                <tr>
                  <th>NAME</th>
                  <th>Arrival Date</th>
                  <th>AEP</th>
                  <th>9G</th>
                  <th>Remarks</th>
                </tr>
              </thead>
              <tbody>
                {visaData.map((item, index) => (
                  <tr key={index}>
                    <td>{item.name}</td>
                    <td>{item.arrivalDate}</td>
                    <td>{`${nineGData[index]?.aepStatus} (${nineGData[index]?.aepDate}) ${nineGData[index]?.aepRemarks}`}</td>
                    <td>{item.status}</td>
                    <td>{item.remarks}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </Card.Text>
        </Card.Body>
        <Card.Footer className='text-muted'></Card.Footer>
      </Card>
    </>
  );
}

export default Visa;
