import React, { useState, useEffect } from 'react';
import { Card, Table } from 'react-bootstrap';

function Vehicle() {
  const [vehicleData, setVehicleData] = useState([]);

  useEffect(() => {
    fetchVehicleData(); 
  }, []);

  const fetchVehicleData = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/general_affairs/display/vehicle`);
      if (response.ok) {
        const data = await response.json();
        setVehicleData(data);
      } else {
        console.error('Failed to fetch data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const calculateDiff = (plan, actual) => plan - actual;

  const getStatus = (diff) => {
    if (diff === 0) {
      return 'MET';
    } else if (diff > 0) {
      return 'DISCREPANCY';
    } else {
      return 'EXCESS';
    }
  };

  return (
    <Card className="text-center card-wrapper" bg='primary' text='light'>
      
      <Card.Body>
        <Card.Title><h1>Vehicles</h1></Card.Title>
        <Card.Text>
          <table className='custom-table'>
            <thead>
              <tr>
                <th>DEPT.</th>
                <th>PLAN</th>
                <th>ACTUAL</th>
                <th>DIFF</th>
                <th>Status</th>
                <th>Remarks</th>
              </tr>
            </thead>
            <tbody>
              {vehicleData.map((data, index) => (
                <tr key={index}>
                  <td>{data.dept}</td>
                  <td>{data.plan}</td>
                  <td>{data.actual}</td>
                  <td>{calculateDiff(data.plan, data.actual)}</td>
                  <td>{getStatus(calculateDiff(data.plan, data.actual))}</td>
                  <td>{data.remarks}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Card.Text>
      </Card.Body>
      <Card.Footer className="text-muted"></Card.Footer>
    </Card>
  );
}

export default Vehicle;
