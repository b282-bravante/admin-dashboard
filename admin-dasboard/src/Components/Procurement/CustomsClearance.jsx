import React, { useEffect, useState } from 'react';
import { Card, Table } from 'react-bootstrap';

function CustomsClearance() {
  const [customsData, setCustomsData] = useState([]);

  useEffect(() => {
    fetchTdCustomsData(); 
  }, []);

  const fetchTdCustomsData = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/procurement/display/customs`);
      if (response.ok) {
        const data = await response.json();
        setCustomsData(data);
      } else {
        console.error('Failed to fetch data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const calculateDiff = (plan, actual) => plan - actual;

  return (
    <Card className="text-center card-wrapper" bg='primary' text='light'>
      
      <Card.Body>
        <Card.Title><h1>Customs Clearance</h1></Card.Title>
        <Card.Text>
          <table className='custom-table'>
            <thead>
              <tr>
                <th>DEPT.</th>
                <th>PLAN</th>
                <th>ACTUAL</th>
                <th>DIFF</th>
                <th>Remarks</th>
              </tr>
            </thead>
            <tbody>
              {customsData.map((data, index) => (
                <tr key={index}>
                  <td>{data.dept}</td>
                  <td>{data.plan}</td>
                  <td>{data.actual}</td>
                  <td>{calculateDiff(data.plan, data.actual)}</td>
                  <td>{data.remarks}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Card.Text>
      </Card.Body>
      <Card.Footer className="text-muted"></Card.Footer>
    </Card>
  );
}

export default CustomsClearance;
