const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://192.168.120.202:3030/', // Your backend server address
      changeOrigin: true,
    })
  );
};