import React, { useState } from 'react';
import { Button, Form, InputGroup, Alert } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

function Register() {
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    userName: '',
    password: '',
    confirmPassword: '',
    department: ''
  });
  const [passwordMatchError, setPasswordMatchError] = useState(false);

  const navigate = useNavigate(); // Use useNavigate instead of useHistory

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (formData.password !== formData.confirmPassword) {
      setPasswordMatchError(true);
      return;
    }

    // Exclude confirmPassword field from formData
    const { confirmPassword, ...dataToSend } = formData;
    console.log(dataToSend)

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(dataToSend)
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);
      navigate('/'); // Use navigate instead of history.push
    })
    .catch(error => console.error('Error registering user:', error));
  };

  return (
    <div className='login-container flex'>
      <div className='login-card'>
        <button className='close-button'>X</button>
        <h1>REGISTER</h1>
        <div className='login-form'>
          <Form onSubmit={handleSubmit}>
            <InputGroup className="mb-3">
              <InputGroup.Text id="inputGroup-sizing-default">
                First Name
              </InputGroup.Text>
              <Form.Control
                type="text"
                name="firstName"
                value={formData.firstName}
                onChange={handleChange}
                aria-label="First Name"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Text id="inputGroup-sizing-default">
                Last Name
              </InputGroup.Text>
              <Form.Control
                type="text"
                name="lastName"
                value={formData.lastName}
                onChange={handleChange}
                aria-label="Last Name"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Text id="inputGroup-sizing-default">
                User
              </InputGroup.Text>
              <Form.Control
                type="text"
                name="userName"
                value={formData.userName}
                onChange={handleChange}
                aria-label="User"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Text id="inputGroup-sizing-default">
                Password
              </InputGroup.Text>
              <Form.Control
                type="password"
                name="password"
                value={formData.password}
                onChange={handleChange}
                aria-label="Password"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Text id="inputGroup-sizing-default">
                Confirm Password
              </InputGroup.Text>
              <Form.Control
                type="password"
                name="confirmPassword"
                value={formData.confirmPassword}
                onChange={handleChange}
                aria-label="Confirm Password"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Text id="inputGroup-sizing-default">
                Department
              </InputGroup.Text>
              <Form.Select
                name="department"
                value={formData.department}
                onChange={handleChange}
                aria-label="Department"
                aria-describedby="inputGroup-sizing-default"
              >
                <option value="">Select Department</option>
                <option value="Accounting">Accounting</option>
                <option value="General Affairs">General Affairs</option>
                <option value="Human Resource">Human Resource</option>
                <option value="Procurement">Procurement</option>
              </Form.Select>
            </InputGroup>
            {passwordMatchError && <Alert variant="danger">Passwords do not match!</Alert>}
            <Button type='submit' variant='primary'>REGISTER</Button>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default Register;
