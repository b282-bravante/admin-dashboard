import React, { useState, useContext } from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContex';

function Login() {
  const [formData, setFormData] = useState({ userName: '', password: '' });
  const navigate = useNavigate();
  const { setUser } = useContext(UserContext);

  const handleLogin = async (e) => {
    e.preventDefault();

    fetch (`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        userName:formData.userName,
        password: formData.password
      })
    }).then(res => res.json())
    .then(data => {
      console.log (`Login data: ${data}`);

      if (typeof data.access !== "undefined") {
        localStorage.setItem('token', data.access)
        retrieveUserDetails(data.access);


        alert ("Login Successful");
        navigate('/');
        
      } else {
        alert ("Login Failed");
      }

      
    })
    setFormData({
      userName: "",
      password: ""
    })
  }

  const retrieveUserDetails = async (token) => {
    await fetch (`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then (res => res.json())
      .then (data => {
        console.log (`Auth data : ${data}`);
        setUser({
          userName: data.userName,
          department: data.department,
          isAdmin: data.isAdmin
        })
      })
  }

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleClose = (e) => {
    e.preventDefault();
    navigate('/');
  }

  return (
    <div className='login-container flex'>
      <div className='login-card'>
        <button 
          className='close-button'
          onClick={handleClose}>X</button>
        <h1>
          LOGIN
        </h1>
        <div className='login-form'>
          <InputGroup className="mb-3">
            <InputGroup.Text id="inputGroup-sizing-default">
              User
            </InputGroup.Text>
            <Form.Control
              type="text"
              name="userName"
              value={formData.userName}
              onChange={handleChange}
              aria-label="User"
              aria-describedby="inputGroup-sizing-default"
            />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroup.Text id="inputGroup-sizing-default">
              Password
            </InputGroup.Text>
            <Form.Control
              type="password"
              name="password"
              value={formData.password}
              onChange={handleChange}
              aria-label="Password"
              aria-describedby="inputGroup-sizing-default"
            />
          </InputGroup>
          <Button variant='primary' onClick={handleLogin}>LOGIN</Button>
        </div>
      </div>
    </div>
  );
}

export default Login;
