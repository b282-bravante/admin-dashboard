import { Navigate } from 'react-router-dom';
import UserContext from "../UserContex";
import { useContext, useEffect } from 'react';

export default function Logout() {
  const { unsetUser, setUser } = useContext(UserContext);

  useEffect(() => {
    // Clear local storage and reset user state
    unsetUser();
    setUser({ id: null, department: null });
  }, [unsetUser, setUser]);

  return <Navigate to="/login" />;
}
