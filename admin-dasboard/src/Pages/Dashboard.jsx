import React, { useState, useEffect, useMemo } from 'react';
import { Button, Carousel } from 'react-bootstrap';
import Tax from '../Components/Accounting/Tax';
import TdStatus from '../Components/Accounting/TdStatus';
import Contributions from '../Components/HumanResource/Contributions';
import Count from '../Components/HumanResource/Count';
import Hiring from '../Components/HumanResource/Hiring';
import WorkerHiring from '../Components/HumanResource/WorkerHiring';
import KoreanAccom from '../Components/GeneralAffairs/KoreanAccom';
import LocalAccom from '../Components/GeneralAffairs/LocalAccom';
import Vehicle from '../Components/GeneralAffairs/Vehicle';
import CustomsClearance from '../Components/Procurement/CustomsClearance';
import Purchases from '../Components/Procurement/Purchases';
import Visa from '../Components/GeneralAffairs/Visa';

function Dashboard() {
  const departments = useMemo(() => [
    {
      name: 'ACCOUNTING',
      data: [Tax, TdStatus],
      data2: ["Tax", "Td Status"],
    },
    {
      name: 'HUMAN RESOURCE',
      data: [Contributions, Count, Hiring, WorkerHiring],
      data2: ["Contributions", "Count", "Hiring", "WorkerHiring"],
    },
    {
      name: 'GENERAL AFFAIRS',
      data: [KoreanAccom, LocalAccom, Vehicle, Visa],
      data2: ["Korean Accom", "Local Accom", "Vehicle", "Visa"],
    },
    {
      name: 'PROCUREMENTS',
      data: [CustomsClearance, Purchases],
      data2: ["Customs Clearance", "Purchases"],
    },
  ], []);

  const [activeDepartmentIndex, setActiveDepartmentIndex] = useState(0);
  const [activeDataIndex, setActiveDataIndex] = useState(0);
  const [previousActiveIndex, setPreviousActiveIndex] = useState(null);

  useEffect(() => {
    
    const interval = setInterval(() => {
      setActiveDataIndex(prevIndex => {
        const nextIndex = (prevIndex + 1) % departments[activeDepartmentIndex].data.length;
        if (nextIndex === 0) {
          const nextDepartmentIndex = (activeDepartmentIndex + 1) % departments.length;
          setPreviousActiveIndex(activeDepartmentIndex); // Update previous active index
          setActiveDepartmentIndex(nextDepartmentIndex);
        }
        return nextIndex;
      });
    }, 20000);

    return () => clearInterval(interval);
  }, [activeDepartmentIndex, departments]);

  const handleDepartmentChange = (index) => {
    
    setPreviousActiveIndex(activeDepartmentIndex);
    setActiveDepartmentIndex(index);
    setActiveDataIndex(0);
  };

  const handleDataChange = (index) => {
    
    setActiveDataIndex(index);
  };

  return (
    <div className="dashboard-container grid">
      <div className='sidebar'>
        <div className='button-container'>
          {departments.map((department, index) => (
            <Button
              key={department.name}
              className={`side-buttons ${activeDepartmentIndex === index ? 'active' : ''} ${previousActiveIndex === index ? 'previous-active' : ''}`}
              
              onClick={() => handleDepartmentChange(index)}
              active={activeDepartmentIndex === index}
            >
              {department.name}
            </Button>
          ))}
        </div>
      </div>
      <div className='viewport'>
          <div className='viewport-buttons-container'>
            {departments[activeDepartmentIndex].data2.map((name, index) => (
              <Button
                key={index}
                className='viewport-buttons'
                variant='primary'
                onClick={() => handleDataChange(index)}
                active={activeDataIndex === index}
              >
                {name}
              </Button>
            ))}
          </div>

          <Carousel
            interval={60000}
            activeIndex={activeDataIndex}
            onSelect={handleDataChange}
          >
            {/* Map through your data */}
            {departments[activeDepartmentIndex].data.map((DataComponent, index) => (
              <Carousel.Item key={index} className={`carousel-item-transition ${index !== activeDataIndex ? 'hide' : ''}`}>
                <div className='viewport-display flex viewport-render'>
                  <DataComponent />
                </div>
              </Carousel.Item>
            ))}
          </Carousel>
      </div>
    </div>
  );
}

export default Dashboard;
